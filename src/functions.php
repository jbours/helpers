<?php

declare(strict_types=1);

/**
 * Fallback functions for the methods in the classes
 *
 * This package includes Laravel's famous support classes and functions
 *  E.q The array(arr_), collection, string (str_) functions.
 * It may be that some functions not included or not working.
 *
 * @see https://laravel.com/docs/master/helpers
 */

use JBours\Helpers\FontAwesome;
use JBours\Helpers\Form;
use JBours\Helpers\Number\Number;
use JBours\Helpers\Number\Percentage;
use JBours\Helpers\Protocol;
use JBours\Helpers\Sanitizer;
use JBours\Helpers\Str;
use JBours\Helpers\Url;
use JBours\Helpers\Value;

if (!function_exists('sanitizePhoneNumber')) {
    /**
     * Returns a callable phone number.
     *
     * @note This function doesn't cover all type of phone number formats, adjust this function
     * accordingly and report your improvements!
     */
    function sanitizePhoneNumber(string $value): string
    {
        return Sanitizer::callablePhoneNumber($value);
    }
}

if (!function_exists('fileToFontAwesome')) {
    /**
     * @deprecated use fileExtensionToFontAwesomeIcon instead!
     */
    function fileToFontAwesome(string $fileExtension): string
    {
        return FontAwesome::getIconForExtension($fileExtension);
    }
}

if (!function_exists('fileExtensionToFontAwesomeIcon')) {
    /**
     * Fetch Font Awesome icon based on the filename
     *
     * This function is currently based on version Font Awesome 4.7.x
     *
     * @param array<string, string> $additionalMappings
     */
    function fileExtensionToFontAwesomeIcon(string $fileExtension, array $additionalMappings = []): string
    {
        return FontAwesome::getIconForExtension($fileExtension, $additionalMappings);
    }
}

if (!function_exists('limitString')) {
    /**
     * Limit the number of characters in a string.
     */
    function limitString(string $value, int $limit, string $end): string
    {
        return Str::limit($value, $limit, $end);
    }
}

if (!function_exists('limitWords')) {
    /**
     * Limit the number of words in a string.
     */
    function limitWords(string $string, int $limit = 100, string $end = '...'): string
    {
        return Str::words($string, $limit, $end);
    }
}

if (!function_exists('parseTemplate')) {
    /**
     * Simple string parser
     *
     * @param iterable<string, string> $data
     * @param array<string> $exclude
     */
    function parseTemplate(iterable $data, string $template, array $exclude = []): string
    {
        return Str::parseTemplate($data, $template, $exclude);
    }
}

if (!function_exists('isSSL')) {
    /**
     * Checks if server is using SSL
     */
    function isSSL(): bool
    {
        return Protocol::isSSL();
    }
}

if (!function_exists('getServerProtocol')) {
    /**
     * @deprecated use getProtocolAsString instead
     */
    function getServerProtocol(): string
    {
        return Protocol::asString();
    }
}

if (!function_exists('getProtocolAsString')) {
    /**
     * Get the protocol as string
     */
    function getProtocolAsString(): string
    {
        return Protocol::asString();
    }
}


if (!function_exists('isMatch')) {
    /**
     * Checks if values match
     */
    function isMatch(mixed $a, mixed $b): bool
    {
        return Value::isMatch($a, $b);
    }
}

if (!function_exists('isExactMatch')) {
    /**
     * Checks if value and type match
     */
    function isExactMatch(mixed $a, mixed $b): bool
    {
        return Value::isExactMatch($a, $b);
    }
}

if (!function_exists('isSelected')) {
    /**
     * Checks if a form select field is selected
     */
    function isSelected(mixed $a, mixed $b): bool|string
    {
        return Form::isSelected($a, $b);
    }
}

if (!function_exists('isSelectedArray')) {
    /**
     * Checks if a form select field  has multiple selection
     *
     * @param array<int|string, int|string> $array
     */
    function isSelectedArray(mixed $value, array $array): bool|string
    {
        return Form::isSelectedArray($value, $array);
    }
}

if (!function_exists('isChecked')) {
    /**
     * Check if a form checkbox/radio field is checked
     */
    function isChecked(mixed $a, mixed $b): bool|string
    {
        return Form::isChecked($a, $b);
    }
}

if (!function_exists('isCheckedArray')) {
    /**
     * Check if multiple form checkbox/radio field are checked
     *
     * @param array<int|string, int|string> $array
     */
    function isCheckedArray(mixed $value, array $array): bool|string
    {
        return Form::isCheckedArray($value, $array);
    }
}

if (!function_exists('isLoopAble')) {
    /**
     * Check if a value is loop-able (e.g. usable for foreach/for loops or array iterations)
     */
    function isLoopAble(mixed $value): bool
    {
        return Value::isLoopAble($value);
    }
}

if (!function_exists('numberIsOdd')) {
    /**
     * Checks of the numeric value is odd
     */
    function numberIsOdd(int $value): bool
    {
        return Number::isOdd($value);
    }
}

if (!function_exists('numberIsEven')) {
    /**
     * Checks of the numeric value is event
     */
    function numberIsEven(int $value): bool
    {
        return Number::isEven($value);
    }
}

if (!function_exists('getPercentage')) {
    /**
     * Get the percentage of a given value
     */
    function getPercentage(float $value, float $total = 100.0): float
    {
        if ((!$total) <=> 100) {
            Percentage::setTotal($total);
        }
        Percentage::setValue($value);

        return Percentage::asFloat();
    }
}

if (!function_exists('hasWWW')) {
    /**
     * Check if www. is in the url
     */
    function hasWWW(): bool
    {
        return Url::hasWWW();
    }
}
