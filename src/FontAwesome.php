<?php

declare(strict_types=1);

namespace JBours\Helpers;

use function array_key_exists;
use function array_merge;
use function count;

class FontAwesome
{
    /**
     * @var array<string, string>
     */
    protected static array $mappedFileExtensions = [];

    /**
     * Fetch Font Awesome icon based on the filename
     *
     * This function is currently based on version Font Awesome 4.7.x
     *
     * @param array<string, string>  $additionalMappings
     */
    public static function getIconForExtension(string $fileExtension, array $additionalMappings = []): string
    {
        self::getMappedFileExtensions();

        if (count($additionalMappings)) {
            self::addMappings($additionalMappings);
        }

        return self::mappingExist($fileExtension) ? self::$mappedFileExtensions[$fileExtension] : 'fa-file-o';
    }

    /**
     * Add new file <-> icon mappings
     *
     * @param array<string, string> $newMappings
     */
    public static function addMappings(array $newMappings): void
    {
        self::$mappedFileExtensions = array_merge($newMappings, self::$mappedFileExtensions);
    }

    /**
     * Checks if the file <-> icon mapping exists
     */
    protected static function mappingExist(string $fileExtension): bool
    {
        return array_key_exists($fileExtension, self::$mappedFileExtensions);
    }

    /**
     * Get the default icon <-> file mappings
     */
    protected static function getMappedFileExtensions(): void
    {
        self::$mappedFileExtensions = [
            /**
             * MS Word
             */
            'doc' => 'fa-file-word-o',
            'docx' => 'fa-file-word-o',

            /**
             * MS Excel
             */
            'xls' => 'fa-file-excel-o',
            'xlsx' => 'fa-file-excel-o',

            /**
             * MS PowerPoint
             */
            'pstx' => 'fa-powerpoint-o',
            'pst' => 'fa-powerpoint-o',
            'pps' => 'fa-powerpoint-o',

            /**
             * Adobe PDF
             */
            'pdf' => 'fa-file-pdf-o',

            /**
             * Compressed file archives
             */
            'zip' => 'fa-file-archive-o',
            'tar' => 'fa-file-archive-o',
            'tar.gz' => 'fa-file-archive-o',
            '7z' => 'fa-file-archive-o',
            'rar' => 'fa-file-archive-o',

            /**
             * Audio Files
             */
            'wav' => 'fa-file-audio-o',
            'mp3' => 'fa-file-audio-o',
            'm4a' => 'fa-file-audio-o',
            'm4p' => 'fa-file-audio-o',
            'flac' => 'fa-file-audio-o',
            'aiff' => 'fa-file-audio-o',
            'aac' => 'fa-file-audio-o',
            'ogg' => 'fa-file-audio-o',

            /**
             * Image files
             */
            'jpg' => 'fa-file-image-o',
            'jpeg' => 'fa-file-image-o',
            'png' => 'fa-file-image-o',
            'gif' => 'fa-file-image-o',
            'raw' => 'fa-file-image-o',
            'tif' => 'fa-file-image-o',
            'tiff' => 'fa-file-image-o',

            /**
             * Video files
             */
            'mp4' => 'fa-file-video-o',
            'avi' => 'fa-file-video-o',
            'flv' => 'fa-file-video-o',
            'mkv' => 'fa-file-video-o',
            'mpg' => 'fa-file-video-o',
            'mpeg' => 'fa-file-video-o',
            'mov' => 'fa-file-video-o',
            'wmv' => 'fa-file-video-o',
            'swf' => 'fa-file-video-o',

            /**
             * Text file
             */
            'txt' => 'fa-file-text-o',
            'log' => 'fa-file-text-o',
            'rtf' => 'fa-file-text-o',
            'csv' => 'fa-file-text-o',
        ];
    }
}
