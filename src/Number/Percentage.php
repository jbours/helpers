<?php

declare(strict_types=1);

namespace JBours\Helpers\Number;

use function number_format;
use function round;

class Percentage
{
    protected static int|float $value;

    protected static int|float $total = 100;

    public static function setValue(float $value): void
    {
        self::$value = $value;
    }

    public static function setTotal(float $value): void
    {
        self::$total = $value;
    }

    public static function asFloat(): float
    {
        return self::$total / self::$value;
    }

    public static function asDecimal(
        int $decimal = 2,
        string $decimalPoint = '.',
        string $thousandSeparator = ','
    ): string {
        return number_format(self::asFloat(), $decimal, $decimalPoint, $thousandSeparator);
    }

    public static function asInteger(): int
    {
        return (int)round(self::asFloat());
    }
}
