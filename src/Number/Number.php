<?php

declare(strict_types=1);

namespace JBours\Helpers\Number;

class Number
{
    /**
     * Checks of the numeric value is odd
     */
    public static function isOdd(int $value): bool
    {
        return !self::isEven($value);
    }

    /**
     * Checks of the numeric value is even
     */
    public static function isEven(int $value): bool
    {
        return ($value % 2) === 0;
    }
}
