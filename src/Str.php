<?php

declare(strict_types=1);

namespace JBours\Helpers;

use function in_array;
use function str_replace;

class Str extends \Illuminate\Support\Str
{
    /**
     * Simple string parser
     *
     * @todo Check if needs to be subtracted from this class
     * @param iterable<string, string> $data
     * @param array<string>  $exclude
     */
    public static function parseTemplate(iterable $data, string $template, array $exclude = []): string
    {
        foreach ($data as $key => $value) {
            if (in_array($key, $exclude, true)) {
                continue;
            }

            $template = str_replace("%{$key}%", $value, $template);
        }

        return $template;
    }
}
