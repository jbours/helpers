<?php

namespace JBours\Helpers;

use function str_replace;
use function strip_tags;
use function trim;

class Sanitizer
{
    /**
     * Sanitize a string
     */
    public static function string(string $string): string
    {
        $string = trim($string);

        return strip_tags($string);
    }

    /**
     * Returns a callable phone number.
     *
     * @note This function doesn't cover all type of phone number formats, adjust this function
     * accordingly and report your improvements!
     */
    public static function callablePhoneNumber(string|int $value): string
    {
        return str_replace(['-', '—', '–', '+', '(0)', ' '], ['', '', '', '00', '', ''], (string)$value);
    }
}
