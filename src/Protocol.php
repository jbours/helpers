<?php

declare(strict_types=1);

namespace JBours\Helpers;

use function strtolower;

class Protocol
{
    /**
     * Get the protocol as string
     */
    public static function asString(): string
    {
        return self::isSSL() ? 'https' : 'http';
    }

    /**
     * Checks if server is using SSL
     */
    public static function isSSL(): bool
    {
        if (isset($_SERVER['HTTPS'])) {
            if ((int)$_SERVER['HTTPS'] === 1 || strtolower($_SERVER['HTTPS']) === 'on') {
                return true;
            }
        } elseif (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] === '443') {
            return true;
        }

        return false;
    }
}
