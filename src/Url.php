<?php

declare(strict_types=1);

namespace JBours\Helpers;

class Url
{
    /**
     * Check if www. is in the url
     */
    public static function hasWWW(): bool
    {
        return str_contains(haystack: $_SERVER['HTTP_HOST'], needle: 'www.');
    }

    /**
     * Sets the last visited url to the session
     */
    public static function setLastVisited(): void
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];
        }
    }

    /**
     * Gets the last visited url from the session
     */
    public static function getLastVisited(): string
    {
        if (isset($_SESSION['last_url']) && !empty($_SESSION['last_url'])) {
            return $_SESSION['last_url'];
        }

        return '';
    }
}
