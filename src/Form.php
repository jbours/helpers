<?php

declare(strict_types=1);

namespace JBours\Helpers;

use function in_array;

class Form
{
    /**
     * Checks if a form select field is selected
     */
    public static function isSelected(mixed $a, mixed $b): bool|string
    {
        return Value::isMatch($a, $b) ? 'selected' : false;
    }

    /**
     * Checks if a form select field  has multiple selection
     *
     * @param array<int|string, int|string> $array
     */
    public static function isSelectedArray(mixed $value, array $array): bool|string
    {
        return in_array($value, $array, true) ? 'selected' : false;
    }

    /**
     * Check if a form checkbox/radio field is checked
     */
    public static function isChecked(mixed $a, mixed $b): bool|string
    {
        return Value::isMatch($a, $b) ? 'checked' : false;
    }

    /**
     * Check if multiple form checkbox/radio field are checked
     *
     * @param array<int|string, int|string> $array
     */
    public static function isCheckedArray(mixed $value, array $array): bool|string
    {
        return in_array($value, $array, true) ? 'checked' : false;
    }
}
