<?php

declare(strict_types=1);

namespace JBours\Helpers;

use function array_map;
use function func_get_args;
use function is_iterable;
use function is_object;
use JetBrains\PhpStorm\NoReturn;
use stdClass;
use Traversable;

class Value
{
    /**
     * Check if a value is loop-able/traversable (e.g. usable for foreach/for loops or array iterations)
     *
     * @param mixed|stdClass|Traversable|array $value
     */
    public static function isLoopAble($value): bool
    {
        return $value !== null && (is_iterable($value) || is_object($value));
    }

    /**
     * Dump and Die (inspired by Laravel's equivalent)
     *
     * @codeCoverageIgnore
     * @return never
     */
    #[NoReturn]
    public static function dd(mixed $value): void
    {
        array_map(
            static function ($x): void {
                dump($x);
            },
            func_get_args()
        );

        die(1);
    }

    /**
     * Checks if values match
     */
    public static function isMatch(mixed $a, mixed $b): bool
    {
        return $a == $b;
    }

    /**
     * Checks if value and type match
     */
    public static function isExactMatch(mixed $a, mixed $b): bool
    {
        return $a === $b;
    }
}
