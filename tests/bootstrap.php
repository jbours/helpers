<?php

use Composer\Autoload\ClassLoader;

$loader = include dirname(__FILE__, 2) . '/vendor/autoload.php';
$classloader = new ClassLoader();
$classloader->addPsr4('JBours\\Test\\', __DIR__, true);
$classloader->register();
