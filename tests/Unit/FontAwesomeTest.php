<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\FontAwesome;
use PHPUnit\Framework\TestCase;

final class FontAwesomeTest extends TestCase
{
    public function testIGetAnIconForNonMappedExtension(): void
    {
        self::assertEquals('fa-file-o', FontAwesome::getIconForExtension('xxx'));
    }

    public function testIGetAnIconForMappedExtension(): void
    {
        self::assertEquals('fa-file-word-o', FontAwesome::getIconForExtension('docx'));
    }

    public function testIGetAnIconForExtensionWhenOverrideMapping(): void
    {
        self::assertEquals('fa-flx-o', FontAwesome::getIconForExtension('flx', ['flx' => 'fa-flx-o']));
    }
}
