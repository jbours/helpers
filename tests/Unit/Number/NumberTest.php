<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit\Number;

use JBours\Helpers\Number\Number as RealNumber;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    /**
     * @dataProvider oddNumberDataProvider
     */
    public static function testOddNumbers(int $value, bool $expected): void
    {
        self::assertEquals($expected, RealNumber::isOdd($value));
    }

    /**
     * @dataProvider evenNumberDataProvider
     */
    public static function testEvenNumbers(int $value, bool $expected): void
    {
        self::assertEquals($expected, RealNumber::isEven($value));
    }

    /**
     * @return array<int, array<int, bool|int>>
     */
    public function oddNumberDataProvider(): array
    {
        return [
            [1, true],
            [99, true],
            [333, true],
            [2, false],
            [20, false],
            [2010, false],
        ];
    }

    /**
     * @return array<int, array<int, bool|int>>
     */
    public function evenNumberDataProvider(): array
    {
        return [
            [2, true],
            [20, true],
            [2010, true],
            [1, false],
            [99, false],
            [333, false],
        ];
    }
}
