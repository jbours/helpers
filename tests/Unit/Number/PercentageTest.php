<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit\Number;

use JBours\Helpers\Number\Percentage;
use PHPUnit\Framework\TestCase;

class PercentageTest extends TestCase
{
    public function testItReturnsFloat(): void
    {
        Percentage::setValue(15);

        self::assertEquals(6.666667, round(Percentage::asFloat(), 6));
    }

    public function testItReturnsDecimal(): void
    {
        Percentage::setValue(15);

        self::assertEquals(6.67, Percentage::asDecimal());
    }

    public function testICanSetATotalValue(): void
    {
        Percentage::setValue(2);
        Percentage::setTotal(10);

        self::assertEquals(5, Percentage::asDecimal());

        Percentage::setTotal(100);
    }

    public function testItReturnsInteger(): void
    {
        Percentage::setValue(15);

        self::assertEquals(7, Percentage::asInteger());
    }
}
