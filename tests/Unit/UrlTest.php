<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\Url;
use PHPUnit\Framework\TestCase;

final class UrlTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function testUrlContainsWww(): void
    {
        $_SERVER['HTTP_HOST'] = 'www.example.com';

        self::assertTrue(Url::hasWWW());
    }

    public function testIfLastVisitedCanBeSet(): void
    {
        $_SERVER['HTTP_REFERER'] = '/home.html';

        Url::setLastVisited();

        self::assertEquals('/home.html', $_SESSION['last_url']);
    }

    public function testICanGetLastVisitedWhenNotYetSet(): void
    {
        self::assertEquals('', Url::getLastVisited());
    }

    public function testICanGetLastVisitedWhenSet(): void
    {
        $_SESSION['last_url'] = '/home.html';

        self::assertEquals('/home.html', Url::getLastVisited());
    }
}
