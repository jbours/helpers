<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\Sanitizer;
use PHPUnit\Framework\TestCase;

class SanitizerTest extends TestCase
{
    public static function testItCanReturnAnSanitizedDutchPhoneNumber(): void
    {
        self::assertEquals('0031612345678', Sanitizer::callablePhoneNumber('0031 (0)6 123 456 78'));
        self::assertEquals('0031612345678', Sanitizer::callablePhoneNumber('+31 (0)6 123 456 78'));
    }

    public function testStringIsSanitized(): void
    {
        self::assertEquals('abc', Sanitizer::string(' <h1>abc</h1>'));
    }
}
