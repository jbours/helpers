<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\Str;
use PHPUnit\Framework\TestCase;

class StrTest extends TestCase
{
    public static function testItCanParseAStringTemplate(): void
    {
        $data = [
            'name' => 'John Doe',
        ];
        $template = 'My name is %name%.';
        $expected = 'My name is John Doe.';

        self::assertEquals(Str::parseTemplate($data, $template), $expected);
    }

    public static function testItCanParseAStringTemplateAndExcludeKeys(): void
    {
        $data = [
            'name' => 'John Doe',
            'name1' => 'John Doe',
        ];
        $template = 'My name is %name%.';
        $expected = 'My name is John Doe.';
        $excluded = ['name1'];

        self::assertEquals(Str::parseTemplate($data, $template, $excluded), $expected);
    }
}
