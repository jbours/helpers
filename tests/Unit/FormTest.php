<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\Form;
use PHPUnit\Framework\TestCase;

use function range;

class FormTest extends TestCase
{
    public static function testItCanSelectAnSelectOption(): void
    {
        self::assertEquals('selected', Form::isSelected(1, 1));
        self::assertEquals(false, Form::isSelected(0, 1));
    }

    public static function testItCanSelectMultipleSelectOptions(): void
    {
        $haystack = range(0, 9);

        self::assertEquals('selected', Form::isSelectedArray(7, $haystack));
        self::assertEquals(false, Form::isSelectedArray(10, $haystack));
    }

    public static function testItCanSelectASingleRadioOrCheckbox(): void
    {
        self::assertEquals('checked', Form::isChecked(1, 1));
        self::assertEquals(false, Form::isChecked(2, 1));
    }

    public static function testItCanSelectMultipleRadioOrCheckboxes(): void
    {
        $haystack = range(0, 9);

        self::assertEquals('checked', Form::isCheckedArray(3, $haystack));
        self::assertEquals(false, Form::isCheckedArray(-1, $haystack));
    }
}
