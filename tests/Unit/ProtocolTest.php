<?php

namespace JBours\Tests\Helpers\Unit;

use JBours\Helpers\Protocol;
use PHPUnit\Framework\TestCase;

class ProtocolTest extends TestCase
{
    public function testItCanDetectIfIsHttp(): void
    {
        self::assertFalse(Protocol::isSSL());
    }

    public function testItCanDetectIfHttpsIsOn(): void
    {
        $_SERVER['HTTPS'] = 'on';

        self::assertTrue(Protocol::isSSL());
    }

    public function testItCanDetectIfRunningOnPort443(): void
    {
        $_SERVER['SERVER_PORT'] = '443';

        self::assertTrue(Protocol::isSSL());
    }

    public function testItCanReturnHttpAsString(): void
    {
        self::assertEquals('http', Protocol::asString());
    }
}
