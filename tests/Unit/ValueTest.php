<?php

declare(strict_types=1);

namespace JBours\Tests\Helpers\Unit;

use ArrayObject;
use JBours\Helpers\Value;
use PHPUnit\Framework\TestCase;
use stdClass;

class ValueTest extends TestCase
{
    public static function testItCanMatchByValue(): void
    {
        self::assertEquals(true, Value::isMatch(true, true));
        self::assertEquals(true, Value::isMatch(1, true));
        self::assertEquals(true, Value::isMatch('1', true));
        self::assertEquals(true, Value::isMatch('true', true));

        self::assertEquals(true, Value::isMatch(false, false));
        self::assertEquals(true, Value::isMatch(0, false));
        self::assertEquals(true, Value::isMatch('0', false));
        self::assertEquals(false, Value::isMatch('false', false));
    }

    public static function testItCanExactMatchByValueAndType(): void
    {
        self::assertEquals(true, Value::isExactMatch(true, true));
        self::assertEquals(false, Value::isExactMatch(1, true));
        self::assertEquals(false, Value::isExactMatch('1', true));
        self::assertEquals(false, Value::isExactMatch('true', true));

        self::assertEquals(true, Value::isExactMatch(false, false));
        self::assertEquals(false, Value::isExactMatch(0, false));
        self::assertEquals(false, Value::isExactMatch('0', false));
        self::assertEquals(false, Value::isExactMatch('false', false));
    }

    public static function testItCanLoop(): void
    {
        self::assertTrue(Value::isLoopAble([]));
        self::assertTrue(Value::isLoopAble(new stdClass()));
        self::assertTrue(Value::isLoopAble(new ArrayObject()));
    }
}
